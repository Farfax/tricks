# Tricks


## DNSMASQ

1. Check sysctl settings
```
sysctl -w net.ipv6.conf.<IF>.accept_ra=1
sysctl -w net.ipv6.conf.<IF>.accept_ra_pinfo=1
sysctl -w net.ipv6.conf.<IF>.accept_ra_defrtr=1
sysctl -w net.ipv6.conf.<IF>.disable_ipv6=0
sysctl -w net.ipv6.conf.all.disable_ipv6=0
```
Router only
```
sysctl -w net.ipv6.conf.all.forwarding=1
sysctl -w net.ipv4.conf.all.forwarding=1
```
2. Disable not configured firewall, selinux
3. Assign address to interface (on router)
```
nmcli connection add type ethernet con-name internal_network ifname <IFname> ip4 10.100.200.1/24 gw4 10.100.200.1 ip6 db01:800:800::1 
```
4. Install dnsmasq and use dnsmasq config
